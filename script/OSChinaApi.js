var API_HOST = 'http://www.oschina.net/';

var OpenAPI = {
	"news_list":  			API_HOST + "action/api/news_list?catalog=1&pageSize=20",
	"news_hot":  			API_HOST + "action/api/news_list?show=week",
	"blog_list":  			API_HOST + "action/api/blog_list?type=latest&pageSize=20",
	"blog_recommend":  		API_HOST + "action/api/blog_list?type=recommend&pageSize=20",
	"post_list":  			API_HOST + "action/api/post_list?pageSize=20",
	"tweet_list":			API_HOST + "action/api/tweet_list?pageSize=20",
	
	"news_detail":			API_HOST + "action/api/news_detail",
	"blog_detail":			API_HOST + "action/api/blog_detail",
	"post_detail":			API_HOST + "action/api/post_detail",
	"software_detail":		API_HOST + "action/api/software_detail",
	
	"favorite_add":			API_HOST + "action/api/favorite_add",
	"favorite_delete":		API_HOST + "action/api/favorite_delete",
	
	"blogcomment_list":		API_HOST + "action/api/blogcomment_list?pageSize=20",
	"comment_list":			API_HOST + "action/api/comment_list?pageSize=20",
	"software_tweet_list":	API_HOST + "action/api/software_tweet_list?pageSize=20",
	
	"comment_pub":			API_HOST + "action/api/comment_pub",
	
	"search_list":			API_HOST + "action/api/search_list?pageSize=20",
	
	"login":  				API_HOST + "action/api/login_validate",
	"logout":  				API_HOST + "action/user/logout",
	"my_information":  		API_HOST + "action/api/my_information",
	"user_notice":  		API_HOST + "action/api/user_notice",
	"community_report":  	API_HOST + "action/communityManage/report",
};